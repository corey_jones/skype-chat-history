﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SkypeChatHistory
{
    public partial class MainForm : Form, IExceptionHandler
    {
        private readonly Plugin _plugin;

        public MainForm()
        {
            InitializeComponent();

            try
            {
                _plugin = new Plugin(this);
            }
            catch (Exception exception)
            {
                Close();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            _plugin.Dispose();
        }

        public bool HandleComException(COMException exception)
        {
            MessageBox.Show(exception.Message, StatusSkype.Properties.Resources.AppName);
            return false;
        }

        public bool HandleException(Exception exception)
        {
            MessageBox.Show(exception.Message, StatusSkype.Properties.Resources.AppName);
            return true;
        }
    }
}
