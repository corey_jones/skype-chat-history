﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using SKYPE4COMLib;
using Settings = StatusSkype.Properties.Settings;

namespace SkypeChatHistory
{
    public class Plugin : IDisposable
    {
        private readonly IExceptionHandler _exceptionHandler;
        private ISkype _skype;
        private readonly _ISkypeEvents_Event _skypeEvents;
        private readonly History _history;

        public Plugin(IExceptionHandler exceptionHandler)
        {
            try
            {
                _exceptionHandler = exceptionHandler;
                _skype = new Skype();
                _skype.Attach(6);

                _history = new History(
                    Settings.Default.FullHistoryDirPath);

                Process[] processesByName = System.Diagnostics.Process.GetProcessesByName("skype.exe");
                processesByName[0].Exited += (sender, args) =>
                    {
                        _exceptionHandler.HandleException(new Exception("skype exited"));
                    };
                _skypeEvents = (_ISkypeEvents_Event)_skype;
                _skypeEvents.MessageStatus += SkypeEventsOnMessageStatus;

                for (int i = 1; i <= _skype.Friends.Count; ++i)
                {
                    User friend = _skype.Friends[i];
                    _history.AddEntry(friend.Handle);
                }
            }
            catch (COMException exception)
            {
                if (!_exceptionHandler.HandleComException(exception))
                    throw;
            }
        }

        private void SkypeEventsOnMessageStatus(ChatMessage pMessage, TChatMessageStatus status)
        {
            try
            {
                var entryName = pMessage.Chat.DialogPartner;
                _history.UpdateEntry(entryName, pMessage);
            }
            catch (Exception exception)
            {
                if (!_exceptionHandler.HandleException(exception))
                    throw;
            }
        }

        public void Dispose()
        {
            Marshal.ReleaseComObject(_skype);
            _skype = null;
            GC.Collect();
        }
    }
}