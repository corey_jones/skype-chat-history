﻿using System;
using System.IO;
using System.Reflection;

namespace StatusSkype.Properties
{
    internal partial class Settings
    {
        public string FullHistoryDirPath
        {
            get
            {
                if (Path.IsPathRooted(HistoryDir))
                    return HistoryDir;

                var location = Path.GetDirectoryName(
                    Assembly.GetExecutingAssembly().Location);
                if (string.IsNullOrEmpty(location))
                    throw new Exception();

                return Path.Combine(location, HistoryDir);
            }
        }
    }
}