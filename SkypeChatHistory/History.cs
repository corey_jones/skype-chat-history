﻿using System;
using System.Collections.Generic;
using System.IO;
using SKYPE4COMLib;

namespace SkypeChatHistory
{
    public class History
    {
        private readonly string _directory;
        private Dictionary<string, HistoryEntry> _historyEntries;

        public History(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            _directory = directory;
            _historyEntries = new Dictionary<string, HistoryEntry>();
        }

        public HistoryEntry AddEntry(string name)
        {
            if (_historyEntries.ContainsKey(name))
                return _historyEntries[name];

            var entryFileName = Path.Combine(
                _directory, ExceptInvalidPathChars(name));
            return _historyEntries[name] = 
                new HistoryEntry(entryFileName);
        }

        public HistoryEntry UpdateEntry(string name, IChatMessage message)
        {
            if (!_historyEntries.ContainsKey(name))
                throw new ArgumentException("name");
            var entry = _historyEntries[name];
            entry.Update(message);
            return entry;
        }

        private static string ExceptInvalidPathChars(string filename)
        {
            foreach (var ch in Path.GetInvalidFileNameChars())
            {
                filename = filename.Replace(ch, '#');
            }

            return filename;
        }
    }
}
